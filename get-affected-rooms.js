const path = require('path');
const fs = require('fs').promises;

(async () => {
  const fileString = await fs.readFile(
    'C:\\Users\\MLM\\Downloads\\matrix-plumbed-room-migration-full-terminal-2021-1-19.txt',
    { encoding: 'utf-8' }
  );

  const portalLines = fileString.split(/\r?\n/).filter((line) => {
    if (line.startsWith('Found portal room that already exists as well')) {
      return true;
    }

    return false;
  });

  const affectedRooms = portalLines.map((line) => {
    return line.replace('Found portal room that already exists as well ', '');
  });

  console.log('affectedRooms', affectedRooms.length);

  const affectedRoomMap = affectedRooms.reduce((currentRoomMap, room) => {
    currentRoomMap[room] = true;
    return currentRoomMap;
  }, {});

  const resultantAffectedRooms = Object.keys(affectedRoomMap);

  console.log('resultantAffectedRooms', resultantAffectedRooms.length);

  fs.writeFile(
    path.join(__dirname, './affected-rooms.json'),
    JSON.stringify(resultantAffectedRooms, null, 2)
  );
})();
